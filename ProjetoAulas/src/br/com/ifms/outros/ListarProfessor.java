/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ifms.outros;

import java.awt.GridLayout;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * @author andreqbs
 */
public class ListarProfessor extends JFrame {

    JPanel painelFundo;
    JTable tabela;
    JScrollPane barraRolagem;
    
    String[] colunas = {"Nome",  "Email"};

    Object[][] dados = {
        {"Toto", "otavio@gmail.com"},
        {"Lulu", "luis@hotmail.com"},
        {"Soso", "soso@gmail.com"},
        {"Cricri", "soso@gmail.com"},
        {"Tata", "soso@gmail.com"}
    };

    public ListarProfessor() {
        super("Professores");
    }

    public void criaJanela() {

        painelFundo = new JPanel();
        painelFundo.setLayout(new GridLayout(1, 1));
        
        tabela = new JTable(dados, colunas);
        barraRolagem = new JScrollPane(tabela);
        painelFundo.add(barraRolagem);

        getContentPane().add(painelFundo);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 120);
        setVisible(true);
    }

    public static void main(String[] args) {
        ListarProfessor lc = new ListarProfessor();
        lc.criaJanela();
    }
}
