package br.com.ifms.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PrimeiraTelaView {

    private JPanel painelGeral;

    private JFrame janela;
    private JButton agenda;
    private JButton professor;
    private JButton turma;
    private JButton solicitacaoAusencia;
    private JButton solicitacaoSubstituicao;
    private JPanel painelOeste;

    public void construir() {
        janela = new JFrame("Sistema Substituição");
        janela.setBounds(0, 0, 600, 250);
        janela.getContentPane().setLayout(new BorderLayout());

        painelGeral = new JPanel();
        painelOeste = new JPanel(new GridLayout(5, 1));
        agenda = new JButton("Agenda");
        professor = new JButton("Professor");
        turma = new JButton("Turma");
        solicitacaoAusencia = new JButton("Solicitção ausência");
        solicitacaoSubstituicao = new JButton("Solicitação substituição");

        painelOeste.add(agenda);
        painelOeste.add(solicitacaoAusencia);
        painelOeste.add(solicitacaoSubstituicao);
        painelOeste.add(professor);
        painelOeste.add(turma);

        painelGeral.add(painelOeste);

        janela.getContentPane().add(painelGeral, BorderLayout.WEST);
        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        janela.setVisible(true);
        janela.setResizable(false);
        acoesBototes();

    }

    public void acoesBototes() {
        agenda.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                AgendaView j = new AgendaView();
                j.construir();
                //janela.dispose();
            }
        });

    }
}
