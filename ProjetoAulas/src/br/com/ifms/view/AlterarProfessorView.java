package br.com.ifms.view;

import br.com.ifms.controller.ProfessorController;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AlterarProfessorView {

    private JTextField textoNome;
    private JTextField textoEmail;
    private JLabel labelEmail;
    private JLabel labelNome;
    private JFrame janela;
    private JPanel painelGeral;
    private JPanel linhaUm;
    private JPanel linhaDois;
    private JPanel linhaTres;
    private JPanel painelEnviar;
    private JButton enviar;

    private JPanel painelCentro;

    public void construir() {
        janela = new JFrame();
        janela.setBounds(0, 0, 700, 180); 
        janela.getContentPane().setLayout(new FlowLayout());

        painelCentro = new JPanel(new GridLayout(3,1));
   
        textoNome = new JTextField();
        textoEmail = new JTextField();
        labelNome = new JLabel("Insira o nome do professor: ");
        labelEmail = new JLabel("Insira o email do professor: ");
        linhaUm = new JPanel(new GridLayout(1, 2));
        linhaDois = new JPanel(new GridLayout(1, 2));
        linhaTres = new JPanel();
        enviar = new JButton("Enviar");

 
        textoNome.setPreferredSize(new Dimension(300, 30));
        textoEmail.setPreferredSize(new Dimension(300, 30));

        linhaUm.add(labelNome);
        linhaUm.add(textoNome);
        linhaDois.add(labelEmail);
        linhaDois.add(textoEmail);
        linhaTres.add(enviar);

        painelCentro.add(linhaUm);
        painelCentro.add(linhaDois);
        painelCentro.add(linhaTres);

        janela.getContentPane().add(painelCentro);
        janela.setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        acoesBotoes();
        
        janela.setVisible(true);
    }
    public void acoesBotoes() {
        enviar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
               String nome=textoNome.getText();
               String email=textoEmail.getText();
               
               ProfessorController  p = new ProfessorController();
               p.criar(nome, email);
               
            }
        });
    }
}
