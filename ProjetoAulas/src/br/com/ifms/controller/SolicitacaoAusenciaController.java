
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.*;
import java.util.Date;
import java.util.LinkedList;

public class SolicitacaoAusenciaController {
    private SolicitacaoAusenciaModel buscado;
    
    Banco banco = new Banco();
    LinkedList<SolicitacaoAusenciaModel> lista = new LinkedList<SolicitacaoAusenciaModel>();

    public void salvar(String justificativa, Boolean interesseParticular, Boolean semAopoioIfms, 
            String justificativaApoio, Boolean ajusteCargaHoraria, String justificativaAjuste, 
            Date data, Boolean autorizado, Boolean autorizadoComComprovacao, String motivoRecusa, 
            Boolean compatibilidadeHorarioAjuste, Boolean limiteHorasDia, Boolean anexadosDocumentosComprobatórios, 
            Boolean anexadoPlanoSubstituicao, String nome, int matricula, String cargo, String setorLocacao, 
            int telefone, String email) {
       SolicitacaoAusenciaModel solicitacaoAusencia = new SolicitacaoAusenciaModel(justificativa, interesseParticular, semAopoioIfms, justificativaApoio, ajusteCargaHoraria, 
       justificativaAjuste, data, autorizado, autorizadoComComprovacao, motivoRecusa, compatibilidadeHorarioAjuste,limiteHorasDia, anexadosDocumentosComprobatórios, 
       anexadoPlanoSubstituicao, nome, matricula, cargo, setorLocacao, telefone, email);
       lista.add(solicitacaoAusencia);
    }

    public void criar(String justificativa, Boolean interesseParticular, Boolean semAopoioIfms, 
            String justificativaApoio, Boolean ajusteCargaHoraria, String justificativaAjuste, 
            Date data, Boolean autorizado, Boolean autorizadoComComprovacao, String motivoRecusa, 
            Boolean compatibilidadeHorarioAjuste, Boolean limiteHorasDia, Boolean anexadosDocumentosComprobatórios, 
            Boolean anexadoPlanoSubstituicao, String nome, int matricula, String cargo, String setorLocacao, 
            int telefone, String email) {
       SolicitacaoAusenciaModel solicitacaoAusencia = new SolicitacaoAusenciaModel(justificativa, interesseParticular, semAopoioIfms, justificativaApoio, ajusteCargaHoraria, 
       justificativaAjuste, data, autorizado, autorizadoComComprovacao, motivoRecusa, compatibilidadeHorarioAjuste,limiteHorasDia, anexadosDocumentosComprobatórios, 
       anexadoPlanoSubstituicao, nome, matricula, cargo, setorLocacao, telefone, email);
       lista.add(solicitacaoAusencia);
    }

    public void alterar(SolicitacaoAusenciaModel velha, SolicitacaoAusenciaModel nova) {
        buscar(velha);
        lista.remove(velha);
        lista.add(nova);
    }

    public void excluir(SolicitacaoAusenciaModel solicitacaoAusencia) {
        buscar(solicitacaoAusencia);
        lista.remove(solicitacaoAusencia);
    }

    public LinkedList listar(LinkedList lista) {
        return lista;
    }

    public SolicitacaoAusenciaModel buscar(SolicitacaoAusenciaModel solicitacaoAusencia) {
        for (int i = 0; i < lista.size(); i++) {
            if (solicitacaoAusencia.equals(lista.get(i))) {
                buscado = solicitacaoAusencia;
            } else//queriamos saber se pode adicionar um comentário quando não encontrar a disciplina buscada
            {
                buscado = null;
            }
        }
        return buscado;
    }
}
