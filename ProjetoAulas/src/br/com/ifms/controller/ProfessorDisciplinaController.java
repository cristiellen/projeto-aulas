
package br.com.ifms.controller;

import br.com.ifms.db.Banco;
import br.com.ifms.model.*;
import br.com.ifms.model.TurmaModel;
import java.util.LinkedList;

public class ProfessorDisciplinaController {
    private ProfessorDisciplinaModel buscado;
    
    Banco banco = new Banco();
    LinkedList<ProfessorDisciplinaModel> lista = new LinkedList<ProfessorDisciplinaModel>();

    public void salvar(ProfessorModel professor, DisciplinaModel disciplina, TurmaModel turma) {
       ProfessorDisciplinaModel professorDisciplina = new ProfessorDisciplinaModel(professor,disciplina, turma );
        lista.add(professorDisciplina);
    }

    public void criar(ProfessorModel professor, DisciplinaModel disciplina, TurmaModel turma) {
       ProfessorDisciplinaModel professorDisciplina = new ProfessorDisciplinaModel(professor,disciplina, turma );
        lista.add(professorDisciplina);
    }

    public void alterar(ProfessorDisciplinaModel velha, ProfessorDisciplinaModel nova) {
        buscar(velha);
        lista.remove(velha);
        lista.add(nova);
    }

    public void excluir(ProfessorDisciplinaModel professorDisciplina) {
        buscar(professorDisciplina);
        lista.remove(professorDisciplina);
    }

    public LinkedList listar(LinkedList lista) {
        return lista;
    }

    public ProfessorDisciplinaModel buscar(ProfessorDisciplinaModel professorDisciplina) {
        for (int i = 0; i < lista.size(); i++) {
            if (professorDisciplina.equals(lista.get(i))) {
                buscado = professorDisciplina;
            } else//queriamos saber se pode adicionar um comentário quando não encontrar a disciplina buscada
            {
                buscado = null;
            }
        }
        return buscado;
    }
}
