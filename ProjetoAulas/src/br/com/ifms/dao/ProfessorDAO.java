package br.com.ifms.dao;

import br.com.ifms.conexao.DBConexao;
import br.com.ifms.model.ProfessorModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ProfessorDAO {

    private final String INSERT = "INSERT INTO PROFESSOR (NOME,  EMAIL) "
            + "VALUES (?,?)";
    private final String UPDATE = "UPDATE PROFESSOR SET NOME=?, EMAIL=? ";
    private final String DELETE = "DELETE FROM PROFESSOR WHERE ID =?";
    private final String LIST = "SELECT * FROM PROFESSOR";
    private final String LISTBYID = "SELECT * FROM PROFESSOR WHERE ID=?";
    private final String CONTAR = "SELECT COUNT(*) as total FROM PROFESSOR";
    private final String LISTNOMEEMAIL = "SELECT * FROM PROFESSOR WHERE NOME=? AND EMAIL=?";

    public void inserir(ProfessorModel professor) {
        if (professor != null) {
            Connection conn = null;
            try {
                conn = DBConexao.getConexao();
                PreparedStatement pstm;
                pstm = conn.prepareStatement(INSERT);

                pstm.setString(1, professor.getNome());
                pstm.setString(2, professor.getEmail());

                pstm.execute();
                JOptionPane.showMessageDialog(null, "professor cadastrado com sucesso");
                DBConexao.fechaConexao(conn, pstm);

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao inserir professor no banco de"
                        + "dados " + e.getMessage());
            }
        } else {
            System.out.println("O professor enviado por parâmetro está vazio");
        }
    }
/*
    public void atualizar(ProfessorModel professor) {
        if (professor != null) {
            Connection conn = null;
            try {
                conn = DBConexao.getConexao();
                PreparedStatement pstm;
                pstm = conn.prepareStatement(UPDATE);
                // pstm.setInt(1, professor.getId());
                pstm.setString(1, professor.getNome());
                pstm.setString(2, professor.getEmail());

                pstm.execute();
                JOptionPane.showMessageDialog(null, "professor alterado com sucesso");
                DBConexao.fechaConexao(conn);

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao atualizar professor no banco de"
                        + "dados " + e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(null, "O professor enviado por parâmetro está vazio");
        }

    }*/

    public void remover(int id) {
        Connection conn = null;
        try {
            conn = DBConexao.getConexao();
            PreparedStatement pstm;
            pstm = conn.prepareStatement(DELETE);

            pstm.setInt(1, id);

            pstm.execute();
            JOptionPane.showMessageDialog(null, "OK");
            // DBConexao.fechaConexao(conn, pstm);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir professor do banco de"
                    + "dados " + e.getMessage());
        }
    }
/*
    public List<ProfessorModel> getProfessores() {

        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        ArrayList<ProfessorModel> professores = new ArrayList<ProfessorModel>();
        try {

            conn = DBConexao.getConexao();
            pstm = conn.prepareStatement(LIST);
            rs = pstm.executeQuery();
            while (rs.next()) {

                ProfessorModel professor = new ProfessorModel();
                professor.setId(rs.getInt("id"));
                System.out.println(professor.getId());
                professor.setNome(rs.getString("nome"));
                System.out.println(professor.getNome());
                professor.setEmail(rs.getString("email"));
                System.out.println(professor.getEmail());
                professores.add(professor);
                
            }
            DBConexao.fechaConexao(conn, pstm, rs);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao listar contatos"
                    + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException e) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return professores;
    }

    public ProfessorModel getProfessorById(int id) {
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        ProfessorModel professor = new ProfessorModel();
        try {
            conn = DBConexao.getConexao();
            pstm = conn.prepareStatement(LISTBYID);
            pstm.setInt(1, id);
            rs = pstm.executeQuery();
            while (rs.next()) {
                professor.setId(rs.getInt("id"));
                professor.setNome(rs.getString("nome"));
                professor.setEmail(rs.getString("email"));
              
            }
            DBConexao.fechaConexao(conn, pstm, rs);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao listar professores" + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return professor;
    }

    public int getQuantidade() {
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs;
        int valor = 0;
        try {
            conn = DBConexao.getConexao();
            pstm = conn.prepareStatement(CONTAR);
            rs = pstm.executeQuery();
            while (rs.next()) {

                valor = rs.getInt("total");
            }
            DBConexao.fechaConexao(conn, pstm, rs);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao listar professores"
                    + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException e) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return valor;
    }

    public ProfessorModel getProfessorNomeEmail(ProfessorModel c) {
        Connection conn = null;
        PreparedStatement pstm = null;
        ResultSet rs = null;
        ProfessorModel professor = new ProfessorModel();
        try {
            conn = DBConexao.getConexao();
            pstm = conn.prepareStatement(LISTNOMEEMAIL);
            pstm.setString(1, c.getNome());
            rs = pstm.executeQuery();
            while (rs.next()) {
                professor.setId(rs.getInt("id"));
                professor.setNome(rs.getString("nome"));
                professor.setEmail(rs.getString("email"));

            }
            DBConexao.fechaConexao(conn, pstm, rs);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao listar professores" + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(ProfessorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return professor;
    }

    public static void main(String[] args) {
        ProfessorDAO cdao = new ProfessorDAO();
        ProfessorModel novo = new ProfessorModel();

        novo.setNome("CHIN");
        novo.setEmail("samaraa@ifms.edu.BR");

        //cdao.inserir(novo);
        //cdao.atualizar(novo);
        //cdao.remover(1);
        // int quantiadde = cdao.getQuantidade();
        // System.out.println(quantiadde);
       // cdao.getProfessores();
       
        // AQUIIII cdao.getProfessorById(novo.getId());
        
        cdao.getProfessorNomeEmail(novo);
        
    }*/

}
