package br.com.ifms.model;

import java.sql.Time;
import java.util.Date;

public class ItemAgendaModel {

    private Time hora;
    private Date data;
    private ProfessorModel professor;
    private TurmaModel turma;

    public ItemAgendaModel(Time hora, Date data, ProfessorModel professor, TurmaModel turma) {
        this.hora = hora;
        this.data = data;
        this.professor = professor;
        this.turma = turma;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public ProfessorModel getProfessor() {
        return professor;
    }

    public void setProfessor(ProfessorModel professor) {
        this.professor = professor;
    }

    public TurmaModel getTurma() {
        return turma;
    }

    public void setTurma(TurmaModel turma) {
        this.turma = turma;
    }


  

}
