
package br.com.ifms.model;


public class ProfessorDisciplinaModel {
    private ProfessorModel professor;
    private DisciplinaModel disciplina;
    private TurmaModel turma;

    public ProfessorDisciplinaModel(ProfessorModel professor, DisciplinaModel disciplina, TurmaModel turma) {
        this.professor = professor;
        this.disciplina = disciplina;
        this.turma = turma;
    }

    public ProfessorModel getProfessor() {
        return professor;
    }

    public void setProfessor(ProfessorModel professor) {
        this.professor = professor;
    }

    public DisciplinaModel getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(DisciplinaModel disciplina) {
        this.disciplina = disciplina;
    }

    public TurmaModel getTurma() {
        return turma;
    }

    public void setTurma(TurmaModel turma) {
        this.turma = turma;
    }
    
    
}
