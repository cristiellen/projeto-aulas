package br.com.ifms.model;

import java.util.List;

public class AgendaModel {

    private List<ItemAgendaModel> listaDeItens;
    private int ano;
    private String nome;

    public AgendaModel(List<ItemAgendaModel> listaDeItens, int ano, String nome) {
        this.listaDeItens = listaDeItens;
        this.ano = ano;
        this.nome = nome;
    }

    public List<ItemAgendaModel> getListaDeItens() {
        return listaDeItens;
    }

    public void setListaDeItens(List<ItemAgendaModel> listaDeItens) {
        this.listaDeItens = listaDeItens;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
