
package br.com.ifms.model;

import java.util.Date;


public class SolicitacaoAusenciaModel extends FormularioSolicitacaoModel{
   
    private String justificativa;
    private Boolean interesseParticular;
    private Boolean semAopoioIfms;
    private String justificativaApoio;
    private Boolean ajusteCargaHoraria;
    private String justificativaAjuste;
    private Date data;
    private Boolean autorizado;
    private Boolean autorizadoComComprovacao;
    private String motivoRecusa;
    private Boolean compatibilidadeHorarioAjuste;
    private Boolean limiteHorasDia;
    private Boolean anexadosDocumentosComprobatórios;
    private Boolean anexadoPlanoSubstituicao;

    public SolicitacaoAusenciaModel(String justificativa, Boolean interesseParticular, Boolean semAopoioIfms, String justificativaApoio, Boolean ajusteCargaHoraria, String justificativaAjuste, Date data, Boolean autorizado, Boolean autorizadoComComprovacao, String motivoRecusa, Boolean compatibilidadeHorarioAjuste, Boolean limiteHorasDia, Boolean anexadosDocumentosComprobatórios, Boolean anexadoPlanoSubstituicao, String nome, int matricula, String cargo, String setorLocacao, int telefone, String email) {
        super(nome, matricula, cargo, setorLocacao, telefone, email);
        this.justificativa = justificativa;
        this.interesseParticular = interesseParticular;
        this.semAopoioIfms = semAopoioIfms;
        this.justificativaApoio = justificativaApoio;
        this.ajusteCargaHoraria = ajusteCargaHoraria;
        this.justificativaAjuste = justificativaAjuste;
        this.data = data;
        this.autorizado = autorizado;
        this.autorizadoComComprovacao = autorizadoComComprovacao;
        this.motivoRecusa = motivoRecusa;
        this.compatibilidadeHorarioAjuste = compatibilidadeHorarioAjuste;
        this.limiteHorasDia = limiteHorasDia;
        this.anexadosDocumentosComprobatórios = anexadosDocumentosComprobatórios;
        this.anexadoPlanoSubstituicao = anexadoPlanoSubstituicao;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    public Boolean getInteresseParticular() {
        return interesseParticular;
    }

    public void setInteresseParticular(Boolean interesseParticular) {
        this.interesseParticular = interesseParticular;
    }

    public Boolean getSemAopoioIfms() {
        return semAopoioIfms;
    }

    public void setSemAopoioIfms(Boolean semAopoioIfms) {
        this.semAopoioIfms = semAopoioIfms;
    }

    public String getJustificativaApoio() {
        return justificativaApoio;
    }

    public void setJustificativaApoio(String justificativaApoio) {
        this.justificativaApoio = justificativaApoio;
    }

    public Boolean getAjusteCargaHoraria() {
        return ajusteCargaHoraria;
    }

    public void setAjusteCargaHoraria(Boolean ajusteCargaHoraria) {
        this.ajusteCargaHoraria = ajusteCargaHoraria;
    }

    public String getJustificativaAjuste() {
        return justificativaAjuste;
    }

    public void setJustificativaAjuste(String justificativaAjuste) {
        this.justificativaAjuste = justificativaAjuste;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getAutorizado() {
        return autorizado;
    }

    public void setAutorizado(Boolean autorizado) {
        this.autorizado = autorizado;
    }

    public Boolean getAutorizadoComComprovacao() {
        return autorizadoComComprovacao;
    }

    public void setAutorizadoComComprovacao(Boolean autorizadoComComprovacao) {
        this.autorizadoComComprovacao = autorizadoComComprovacao;
    }

    public String getMotivoRecusa() {
        return motivoRecusa;
    }

    public void setMotivoRecusa(String motivoRecusa) {
        this.motivoRecusa = motivoRecusa;
    }

    public Boolean getCompatibilidadeHorarioAjuste() {
        return compatibilidadeHorarioAjuste;
    }

    public void setCompatibilidadeHorarioAjuste(Boolean compatibilidadeHorarioAjuste) {
        this.compatibilidadeHorarioAjuste = compatibilidadeHorarioAjuste;
    }

    public Boolean getLimiteHorasDia() {
        return limiteHorasDia;
    }

    public void setLimiteHorasDia(Boolean limiteHorasDia) {
        this.limiteHorasDia = limiteHorasDia;
    }

    public Boolean getAnexadosDocumentosComprobatórios() {
        return anexadosDocumentosComprobatórios;
    }

    public void setAnexadosDocumentosComprobatórios(Boolean anexadosDocumentosComprobatórios) {
        this.anexadosDocumentosComprobatórios = anexadosDocumentosComprobatórios;
    }

    public Boolean getAnexadoPlanoSubstituicao() {
        return anexadoPlanoSubstituicao;
    }

    public void setAnexadoPlanoSubstituicao(Boolean anexadoPlanoSubstituicao) {
        this.anexadoPlanoSubstituicao = anexadoPlanoSubstituicao;
    }
    
    
}
