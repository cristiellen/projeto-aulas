
package br.com.ifms.model;

import java.util.ArrayList;
import java.util.List;


public class ProfessorTableModel {
    private static final int COL_ID = 0;
    private static final int COL_NOME = 1;
    private static final int COL_EMAIL = 3;
    
     List<ProfessorModel> linhas;
    private final String[] colunas = new String[] {"Id", "Nome", "Email"};
 
    public ProfessorTableModel(List<ProfessorModel> professores) {
        this.linhas = new ArrayList<>(professores);
    }
 
 
    public int getRowCount() {
        return linhas.size();
    }
 
 
    public int getColumnCount() {
        return colunas.length;
    }
 
   
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }
 
    
    public Class getColumnClass(int columnIndex) {
        if (columnIndex == COL_ID) {
            return Integer.class;
        }
        return String.class;
    }
 
   
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
 
  
    public Object getValueAt(int row, int column) {
        ProfessorModel m = linhas.get(row);
        switch (column) {
            case COL_ID:
                return m.getId();
            case COL_NOME:
                return m.getNome();
            case COL_EMAIL:
                return m.getEmail();
            default:
                break;
        }
        return "";
    }
 

    public void setValueAt(Object aValue, int row, int column) {
        ProfessorModel u = linhas.get(row);
        switch (column) {
            case COL_ID:
                u.setId((Integer) aValue);
                break;
            case COL_NOME:
                u.setNome(aValue.toString());
                break;
            case COL_EMAIL:
                u.setEmail(aValue.toString());
                break;
            default:
                break;
        }
    }
 
    public ProfessorModel getContato(int indiceLinha) {
        return linhas.get(indiceLinha);
    }
 
    public void addContato(ProfessorModel contato) {
        linhas.add(contato);
        //fireTableDataChanged();
 
    }
 
    public void updateContato(int indiceLinha, ProfessorModel marca) {
        linhas.set(indiceLinha, marca);
        //fireTableRowsUpdated(indiceLinha, indiceLinha);
 
    }
 
    public void removeProfessor(int indiceLinha) {
        linhas.remove(indiceLinha);
        //fireTableRowsDeleted(indiceLinha, indiceLinha);
 
    }


}
